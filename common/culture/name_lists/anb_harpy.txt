﻿name_list_firanyan = {

	cadet_dynasty_names = {
		"dynn_nostanthu"
		"dynn_ernoqhantova"
		"dynn_jereron"
		"dynn_mauteron"
		"dynn_saroeron"
		"dynn_vessaeron"
		"dynn_sekhujini"
		"dynn_sekhuvara"
		"dynn_veluvosqantu"
		"dynn_soiokhanthu"
		"dynn_soioosqantu"
		"dynn_soiojakhanthu"
		"dynn_soiosesanthu"
		"dynn_latioqhantova"
		"dynn_vekhiara"
		"dynn_vekhieron"
		"dynn_vekhisekhu"
		"dynn_qhonioenna"
		"dynn_qhoneara"
		"dynn_qhoneeron"
		"dynn_maujali"
		"dynn_thiusali"
		"dynn_thiuvekhi"
		"dynn_thiusekhu"
		"dynn_thiuivene"
		"dynn_saroivene"
		"dynn_pesivene"
		"dynn_honfira"
		"dynn_honnephe"
		"dynn_nephiofira"
		"dynn_nephiosekhu"
		"dynn_nephiosali"
		"dynn_pamoqhantova"
		"dynn_panvelu"
		"dynn_mourveta"
		"dynn_pamoveta"
		"dynn_fillanveta"
		"dynn_nephioveta"
		"dynn_aratoveta"
		"dynn_fisaveta"
		"dynn_vetanos"
		"dynn_vetaeron"
		"dynn_vetaara"
		"dynn_vetafirar"
		"dynn_hytiranyau"
		"dynn_ayarato"
		"dynn_nansau"
		"dynn_misarato"
		"dynn_lysinyau"
		"dynn_firanyau"
		"dynn_relsali"
	}

	dynasty_names = {
		"dynn_nostanthu"
		"dynn_ernoqhantova"
		"dynn_jereron"
		"dynn_mauteron"
		"dynn_saroeron"
		"dynn_vessaeron"
		"dynn_sekhujini"
		"dynn_sekhuvara"
		"dynn_veluvosqantu"
		"dynn_soiokhanthu"
		"dynn_soioosqantu"
		"dynn_soiojakhanthu"
		"dynn_soiosesanthu"
		"dynn_latioqhantova"
		"dynn_vekhiara"
		"dynn_vekhieron"
		"dynn_vekhisekhu"
		"dynn_qhonioenna"
		"dynn_qhoneara"
		"dynn_qhoneeron"
		"dynn_maujali"
		"dynn_thiusali"
		"dynn_thiuvekhi"
		"dynn_thiusekhu"
		"dynn_thiuivene"
		"dynn_saroivene"
		"dynn_pesivene"
		"dynn_honfira"
		"dynn_honnephe"
		"dynn_nephiofira"
		"dynn_nephiosekhu"
		"dynn_nephiosali"
		"dynn_pamoqhantova"
		"dynn_panvelu"
		"dynn_mourveta"
		"dynn_pamoveta"
		"dynn_fillanveta"
		"dynn_nephioveta"
		"dynn_aratoveta"
		"dynn_fisaveta"
		"dynn_vetanos"
		"dynn_vetaeron"
		"dynn_vetaara"
		"dynn_vetafirar"
		"dynn_hytiranyau"
		"dynn_ayarato"
		"dynn_nansau"
		"dynn_misarato"
		"dynn_lysinyau"
		"dynn_firanyau"
		"dynn_relsali"
	}

	male_names = {
		Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
	}
	female_names = {
		Firanya Sarojini Is_tara Niloheunthu Heunthoevesa Elavyenna S_inenthu Elu S_ananthu Vinkanthu Askina Pomaranthu Najukanthu Telvinkanthu Tinranthu Tenu Nalofanthu Neha Henra Mula
		Henranthu Nilli Eva Mis_qa Herun Rauva Varva Sunqa Isun Tauhun Kemoun Thelater Thelavi Harpa Syphe Ris_pan Tus_akor Pielli Kerria Nala
		Lysi Touna Mons_e Jorouna Lys_koler Sillys_ka Lys_komus Nalaisa Siatuna Sarvatun Vyma Helia Jor Jorvi Vymaukyre Nephe Nemphantu Nephiotessa Nepheer Munnephe
		Nephiojor Kossun Nephiokes Seturno Saero Ienosita Mustys_ Heise Pessitys_ Jarme Iliosita Firaer Astusol Nalayni His_aer His_avi Vainiosse Kellaun Kiris_a Kironephe
		Pessi Hassi Teisi Mursi This_s_i Qhenol Parmae Felvaa Enivene Nehi Maa Teroivi Erunvi Hannu Horse Hannuer Konthu Soievi Vilanthu Oupijei
		Kympun Rifa Nynna Lera Inanko Nenka Ponunus Telunus Nephevi Nehiunus Nephiokestar Telar Tels_ali Ikoutel Esivene S_ite Akolvi Riane Ivinnanthu Is_lananthu
		Is_lanthu Jonsenthu Hytiranya As_uria Relyore Renianta Kevofa Ayara Mis_ara Nans_a Lysinya
	}

	dynasty_of_location_prefix = "dynnp_khen"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10

	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_siadunan = {

	male_names = {
		Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
	}
	female_names = {
		Hirania Saroji_ni Asta_rta Niloiu_nthu Ionthue_za Elaie_nna S_ine_nthu Je_ltu S_ana_nthu Banga_nthu Iaski_na Pomara_nthu Nac_huka_nthu Telanga_nthu Tanra_nthu Te_nu Nalova_nthu Inia Ie_nra Mu_la
		Anra_nthu Ni_li Io_a Imi_sga E_run Ira_ua Ia_ra Izu_nga I_zun Tau_n Kemu_n Thela_der Thela_di A_rpa Si_phe Ri_span Tuja_gor Pie_li Keria Na_la
		Li_zi Tu_na Mo_nje C_hartu_na Liskole_r Sili_ska Liskomu_s Nale_za Siadu_na Sara_dun Bi_ma E_lka C_ho_r C_ho_ri Bimogi_re Ne_phe Nampha_ndu Nephiode_sa Nephe_r Manne_phe
		Nephioc_ho_r Ko_sun Nephioge_s Sedu_rno Se_ro Ienozi_da Mu_stis I_ze Pesi_dis C_a_rme Iliozi_da Hire_r Astu_zol Nale_ni Ije_r Ije_ Benio_se Kelo_n Kiri_ja Kirone_phe
		Pe_si A_si Ti_zi Mu_rsi Thi_si He_nol Parme_ Ivela_ Jonie_ne Ni_ Ma_ Terui_ Jeru_ni A_nnu O_rse Annuer Ko_nthu Soii_ Bila_nthu Ubic_i_
		Ki_mbun Ri_va Ni_nna Ile_ra Ina_ngo Ine_nga Ponu_nus Telu_nus Nephi_ Niunus Nephioge_star Te_ltar Tals_a_li Igude_l Jasie_ne S_i_de Ago_li Ria_ne Anna_nthu Is_lana_nthu
		As_la_nthu C_anze_nthu Idirania Ajuria Relio_re Renia_nda Kiova_ Aia_ra Mija_ra Ina_nja Lizinia
	}

	dynasty_names = {
		"dynn_niastanthu"
		"dynn_ernohandua"
		"dynn_cereron"
		"dynn_moderon"
		"dynn_sarueron"
		"dynn_beseron"
		"dynn_sekhujini"
		"dynn_sekhuara"
		"dynn_beltuasgandu"
		"dynn_soiokhanthu"
		"dynn_soiasgandu"
		"dynn_soiojakhanthu"
		"dynn_soiozezanthu"
		"dynn_ladiohandua"
		"dynn_bekhiara"
		"dynn_bekhieron"
		"dynn_bekhizekhu
		"dynn_honiuenna"
		"dynn_honiara"
		"dynn_honeron"
		"dynn_mojali"
		"dynn_thujali"
		"dynn_thiuekhi"
		"dynn_thuzekhu"
		"dynn_thuiene"
		"dynn_saroiene"
		"dynn_peziene"
		"dynn_anvira"
		"dynn_annephe"
		"dynn_nephiovira"
		"dynn_nephiozekhu"
		"dynn_nephiojali"
		"dynn_pamohandua"
		"dynn_paneltu"
		"dynn_mureda"
		"dynn_pamueda"
		"dynn_hilaneda"
		"dynn_nephiueda"
		"dynn_aradueda"
		"dynn_hizeda"
		"dynn_bedanios"
		"dynn_bederon"
		"dynn_bedera"
		"dynn_bedavirar"
		"dynn_ijozekhu"
		"dynn_talnuma"
		"dynn_kiosireron"
		"dynn_siaduno"
		"dynn_mulo"
		"dynn_elaienno"
	}	
	cadet_dynasty_names = {
		"dynn_niastanthu"
		"dynn_ernohandua"
		"dynn_cereron"
		"dynn_moderon"
		"dynn_sarueron"
		"dynn_beseron"
		"dynn_sekhujini"
		"dynn_sekhuara"
		"dynn_beltuasgandu"
		"dynn_soiokhanthu"
		"dynn_soiasgandu"
		"dynn_soiojakhanthu"
		"dynn_soiozezanthu"
		"dynn_ladiohandua"
		"dynn_bekhiara"
		"dynn_bekhieron"
		"dynn_bekhizekhu
		"dynn_honiuenna"
		"dynn_honiara"
		"dynn_honeron"
		"dynn_mojali"
		"dynn_thujali"
		"dynn_thiuekhi"
		"dynn_thuzekhu"
		"dynn_thuiene"
		"dynn_saroiene"
		"dynn_peziene"
		"dynn_anvira"
		"dynn_annephe"
		"dynn_nephiovira"
		"dynn_nephiozekhu"
		"dynn_nephiojali"
		"dynn_pamohandua"
		"dynn_paneltu"
		"dynn_mureda"
		"dynn_pamueda"
		"dynn_hilaneda"
		"dynn_nephiueda"
		"dynn_aradueda"
		"dynn_hizeda"
		"dynn_bedanios"
		"dynn_bederon"
		"dynn_bedera"
		"dynn_bedavirar"
		"dynn_ijozekhu"
		"dynn_talnuma"
		"dynn_kiosireron"
		"dynn_siaduno"
		"dynn_mulo"
		"dynn_elaienno"
	}
	
	dynasty_of_location_prefix = "dynnp_khen"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10

	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}
