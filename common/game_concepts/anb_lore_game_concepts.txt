﻿war_of_the_sorcerer_king = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

battle_of_balmire = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

battle_of_morban_flats = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

battle_of_trialmount = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

butchers_field_massacre = {
	texture = "gfx/interface/icons/traits/forbidden_magic_practitioner.dds"
}

battle_of_red_reach = {
	texture = "gfx/interface/icons/map_icons/combat_map_icon.dds"
}

culture_description_sun_elves = {
	texture = "gfx/interface/icons/icon_culture.dds"
}

culture_description_moon_elves = {
	texture = "gfx/interface/icons/icon_culture.dds"
}