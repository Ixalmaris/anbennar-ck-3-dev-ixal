﻿###DECISIONS LIST###

# pearlsedger_cut_hair_decision
# repair_carillon_of_the_lunetein

pearlsedger_cut_hair_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_misc.dds"
	}

	desc = pearlsedger_cut_hair_decision_desc
	selection_tooltip = pearlsedger_cut_hair_decision_tooltip

	ai_check_interval = 1

	is_shown = {
		has_culture = culture:pearlsedger
		NOT = {
			has_character_flag = bald_head
		}
	}

	is_valid = {
		OR = {
			trigger_if = {
				limit = {
					is_female = no
				}
				is_married = yes
			}
			trigger_if = {
				limit = {
					is_female = yes
				}
				martial >= 30
			}
			trigger_else = {
				martial >= 20
			}
			trigger_if = {
				limit = {
					is_female = yes
				}
				prowess >= 30
			}
			trigger_else = {
				prowess >= 20
			}
		}
	}

	effect = {
		hidden_effect = {
			add_character_flag = {
				flag = bald_head
				days = -1
			}
		}

		trigger_event =  anb_minor_decisions.0001
		custom_tooltip = pearlsedger_cut_hair_decision_effect_tooltip
	}
	
	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 4000 # They'll always do it
	}
}

repair_carillon_of_the_lunetein = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_smith.dds"
	}
	desc = repair_carillon_of_the_lunetein_desc

	is_shown = {
		any_character_artifact = {
			has_variable = is_carillon_of_the_lunetein
		}
	}
	
	is_valid = {
		culture = { has_cultural_era_or_later = culture_era_high_medieval }
	}
	
	cost = {
		gold = 1000
	}

	effect = {
		custom_tooltip = repair_carillon_of_the_lunetein_effect # repair the carillon permanently
		
		trigger_event = anb_minor_decisions.0003 # some event that give some lore + some renown and prestige ?
	}

	ai_check_interval = 60
	
	ai_potential = {
		
	}

	ai_will_do = {
		base = 1000
	}
}
