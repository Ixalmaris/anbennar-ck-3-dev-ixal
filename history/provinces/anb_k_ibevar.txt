#k_ibevar
##d_ibevar
###c_ibevar
352 = {		#Ibevar

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1705 = {

    # Misc
    holding = church_holding

    # History

}
1706 = {

    # Misc
    holding = city_holding

    # History

}
1707 = {

    # Misc
    holding = none

    # History

}
1708 = {

    # Misc
    holding = none

    # History

}

###c_varillen
353 = {		#Varillen

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1713 = {

    # Misc
    holding = city_holding

    # History

}
1714 = {

    # Misc
    holding = none

    # History

}
1715 = {

    # Misc
    holding = none

    # History

}

###c_ar_einnas
356 = {		#Ar Einnas

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1709 = {

    # Misc
    holding = none

    # History

}
1710 = {

    # Misc
    holding = church_holding

    # History

}
1711 = {

    # Misc
    holding = city_holding

    # History

}
1712 = {

    # Misc
    holding = none

    # History

}

###c_ainethil
359 = {		#Ainethil

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1716 = {

    # Misc
    holding = church_holding

    # History

}
1721 = {

    # Misc
    holding = none

    # History

}

##d_nurael
###c_nurael
351 = {		#Nurael

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1701 = {

    # Misc
    holding = church_holding

    # History

}
1702 = {

    # Misc
    holding = none

    # History

}

###c_havoral_peak
350 = {		#Havoral Peak

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}

###c_carodirs_pass
261 = {		#Carodir's Pass

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1700 = {

    # Misc
    holding = none

    # History

}

###c_thanas
360 = {		#Thanas

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1703 = {

    # Misc
    holding = city_holding

    # History

}
1704 = {

    # Misc
    holding = none

    # History

}

##d_larthan
###c_larthan
354 = {		#Larthan

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1717 = {

    # Misc
    holding = church_holding

    # History

}
1718 = {

    # Misc
    holding = none

    # History

}

###c_blaiddscal
361 = {		#Blaiddscal

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1719 = {

    # Misc
    holding = city_holding

    # History

}
1720 = {

    # Misc
    holding = none

    # History

}

###c_tombsvale
422 = {		#Tombsvale

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
1725 = {

    # Misc
    holding = none

    # History

}

##d_whistlevale
###c_escin
777 = {		#Escin

    # Misc
    culture = esmari
    religion = cult_of_esmaryal
	holding = castle_holding

    # History
}
2229 = {

    # Misc
    holding = church_holding

    # History

}
2230 = {

    # Misc
    holding = city_holding

    # History

}

###c_whistlehill
779 = {		#Whistlehill

    # Misc
    culture = adeanic
    religion = adenican_adean
	holding = castle_holding

    # History
}
2231 = {

    # Misc
    holding = city_holding

    # History

}
2232 = {

    # Misc
    holding = none

    # History

}

###c_hushdell
778 = {		#Hushdell

    # Misc
    culture = adeanic
    religion = adenican_adean
	holding = castle_holding

    # History
}
2233 = {

    # Misc
    holding = church_holding

    # History

}
2234 = {

    # Misc
    holding = none

    # History

}

###c_lonesfield
775 = {		#Lonesfield

    # Misc
    culture = adeanic
    religion = adenican_adean
	holding = castle_holding

    # History
}
2235 = {

    # Misc
    holding = city_holding

    # History

}
2236 = {

    # Misc
    holding = none

    # History

}

##d_cursewood
###c_hexfort
257 = {		#Hexfort

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
2241 = {

    # Misc
    holding = city_holding

    # History

}
2242 = {

    # Misc
    holding = none

    # History

}

###c_coldrest
358 = {		#Coldrest

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
2239 = {

    # Misc
    holding = church_holding

    # History

}
2240 = {

    # Misc
    holding = none

    # History

}

###c_luckless_redoubt
355 = {		#Luckless Redoubt

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
2243 = {

    # Misc
    holding = city_holding

    # History

}
2244 = {

    # Misc
    holding = none

    # History

}

###c_freetower
248 = {		#Freetower

    # Misc
    culture = moon_elvish
    religion = elven_forebears
	holding = castle_holding

    # History
}
2245 = {

    # Misc
    holding = none

    # History

}
2246 = {

    # Misc
    holding = city_holding

    # History

}
