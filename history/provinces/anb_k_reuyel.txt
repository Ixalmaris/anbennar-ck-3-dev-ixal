﻿#k_reuyel
##d_reuyel
###c_reuyel
521 = {		#Re-Uyel

	# Misc
	culture = yametsesi
	religion = jaherian_cults
	holding = castle_holding

	# History
}
2836 = {	#Eduz-Suraqqu

	# Misc
	holding = church_holding

	# History

}
2837 = {	#Bittuathan

	# Misc
	holding = city_holding

	# History

}
2838 = {	#Akurmilu

	# Misc
	holding = none

	# History

}
2839 = {	#Azkapas

	# Misc
	holding = none

	# History

}
2851 = {	#Azkeru

	# Misc
	holding = city_holding

	# History

}

###c_azkaszelazka
522 = {		#Azka-szel-Azka

	# Misc
	culture = yametsesi
	religion = cult_of_the_citadel
	holding = castle_holding

	# History
}
2840 = {	#Eduz-Azka

	# Misc
	religion = cult_of_the_citadel
	holding = church_holding

	# History

}
2841 = {	#Azkalis

	# Misc
	religion = cult_of_the_citadel
	holding = city_holding

	# History

}
2842 = {	#Arzdisit

	# Misc
	religion = cult_of_the_citadel
	holding = none

	# History

}

###c_yametses
523 = {		#Zorntanas

	# Misc
	culture = sun_elvish
	religion = sundancer_paragons
	holding = castle_holding

	# History
}
581 = {		#Yametses

	# Misc
	culture = sun_elvish
	religion = sundancer_paragons
	holding = city_holding

	# History
}
2843 = { 	#Eduz-Arzdibaga

	# Misc
	culture = sun_elvish
	religion = sundancer_paragons
	holding = church_holding

	# History

}

##d_medbahar
###c_kaproya_telen
528 = {		#Kaproya-Telen

	# Misc
	culture = bahari
	religion = cult_of_the_eseral_mitellu
	holding = castle_holding

	# History
}
2844 = {	#Eduz-Ereskand

	# Misc
	culture = bahari
	religion = cult_of_the_eseral_mitellu
	holding = church_holding

	# History

}
2845 = {	#Arzdiqqu

	# Misc
	culture = bahari
	religion = cult_of_the_eseral_mitellu
	holding = none

	# History

}

###c_azka_barzil
529 = {		#Azka Barzil

	# Misc
	culture = yametsesi
	religion = cult_of_the_eseral_mitellu
	holding = castle_holding

	# History
}
2846 = {	#Baharthal

	# Misc
	religion = cult_of_the_eseral_mitellu
	holding = city_holding

	# History

}
2847 = {	#Barzisim

	# Misc
	religion = cult_of_the_eseral_mitellu
	holding = none

	# History

}

###c_akrad_til
520 = {		#Akrad-til

	# Misc
	culture = yametsesi
	religion = cult_of_the_citadel
	holding = castle_holding

	# History
}
2848 = {	#Eduz-Ituq

	# Misc
	religion = cult_of_the_citadel
	holding = church_holding

	# History

}
2849 = {	#Kallabar

	# Misc
	religion = cult_of_the_citadel
	holding = none

	# History

}
2850 = {	#Tilabar

	# Misc
	religion = cult_of_the_citadel
	holding = none

	# History

}

##d_kisakur
###c_kisakur
516 = {		#Kisakur

	# Misc
	culture = yametsesi
	religion = cult_of_the_lightfather
	holding = castle_holding

	# History
}
2823 = {	#Eduz-Lara

	# Misc
	religion = cult_of_the_lightfather
	holding = church_holding

	# History

}
2824 = {	#Katrano

	# Misc
	religion = cult_of_the_lightfather
	holding = city_holding

	# History

}

###c_sirafan
517 = {		#Sirafan

	# Misc
	culture = yametsesi
	religion = cult_of_the_eseral_mitellu
	holding = castle_holding

	# History
}
2825 = {	#Kursad

	# Misc
	religion = cult_of_the_eseral_mitellu
	holding = none

	# History

}

###c_arekis
518 = {		#Arekis

	# Misc
	culture = yametsesi
	religion = cult_of_the_citadel
	holding = castle_holding

	# History
}
2826 = {	#Disfan

	# Misc
	religion = cult_of_the_citadel
	holding = church_holding

	# History

}
2827 = {	#Libbures

	# Misc
	religion = cult_of_the_citadel
	holding = none

	# History

}

###c_gemisle
452 = {		#Gemisle

	# Misc
	culture = ilatani
	religion = cult_of_the_lightfather
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = gemisle_mines_01
	}
}
2828 = {	#Ginusah

	# Misc
	holding = none

	# History

}
2852 = {	#Eduz-Arameh

	# Misc
	holding = church_holding

	# History

}
2853 = {	#Ar-sawad

	# Misc
	holding = city_holding

	# History

}
2854 = {	#Uthojstunad

	# Misc
	holding = none

	# History

}