#k_bjarnrik
##d_bjarnland
###c_konugrhavn
956 = {		#Konugrhavn

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2740 = {

    # Misc
    holding = church_holding

    # History

}
2741 = {

    # Misc
    holding = city_holding

    # History

}
2742 = {

    # Misc
    holding = none

    # History

}
2743 = {

    # Misc
    holding = none

    # History

}
2744 = {

    # Misc
    holding = none

    # History

}

###c_torbjoldhavn
955 = {		#Torbjoldhavn

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2745 = {

    # Misc
    holding = city_holding

    # History

}
2746 = {

    # Misc
    holding = none

    # History

}

###c_nautakr
954 = {		#Nautakr

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2738 = {

    # Misc
    holding = city_holding

    # History

}
2739 = {

    # Misc
    holding = none

    # History

}

##d_bifrutja
###c_sporreyn
952 = {		#Sporreyn

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2759 = {

    # Misc
    holding = church_holding

    # History

}
2760 = {

    # Misc
    holding = none

    # History

}
2761 = {

    # Misc
    holding = none

    # History

}

###c_jotunsglotta
708 = {		#Jotunsglotta

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2762 = {

    # Misc
    holding = none

    # History

}

###c_fuglborg
953 = {		#Fuglborg

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2763 = {

    # Misc
    holding = city_holding

    # History

}
2764 = {

    # Misc
    holding = none

    # History

}
2765 = {

    # Misc
    holding = none

    # History

}

##d_revrland
###c_revrhavn
959 = {		#Revrhavn

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2750 = {

    # Misc
    holding = city_holding

    # History

}
2751 = {

    # Misc
    holding = church_holding

    # History

}
2752 = {

    # Misc
    holding = none

    # History

}
2753 = {

    # Misc
    holding = none

    # History

}
2754 = {

    # Misc
    holding = none

    # History

}

###c_eikodr
997 = {		#Eikodr

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2747 = {

    # Misc
    holding = church_holding

    # History

}
2748 = {

    # Misc
    holding = none

    # History

}
2749 = {

    # Misc
    holding = none

    # History

}

###c_vestrund
960 = {		#Vestrund

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2755 = {

    # Misc
    holding = city_holding

    # History

}
2756 = {

    # Misc
    holding = none

    # History

}
2757 = {

    # Misc
    holding = none

    # History

}
2758 = {

    # Misc
    holding = none

    # History

}

###c_knarhatt
993 = {		#Knarhatt

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}

##d_haugrmyrr
###c_haugrmyrr
961 = {		#Haugrmyrr

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2768 = {

    # Misc
    holding = church_holding

    # History

}
2769 = {

    # Misc
    holding = none

    # History

}
2770 = {

    # Misc
    holding = none

    # History

}

###c_leifsborg
962 = {		#Leifsborg

    # Misc
    culture = dalric
    religion = old_gerudian_faith
    holding = castle_holding

    # History
}
2771 = {

    # Misc
    holding = city_holding

    # History

}
2772 = {

    # Misc
    holding = none

    # History

}
2773 = {

    # Misc
    holding = none

    # History

}

##d_sarbann
###c_sarbann
963 = {		#Sarbann

    # Misc
    culture = dalric
    religion = old_gerudian_faith
    holding = castle_holding

    # History
}
2774 = {

    # Misc
    holding = none

    # History

}
2775 = {

    # Misc
    holding = none

    # History

}

###c_fegras
958 = {		#Fegras

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2776 = {

    # Misc
    holding = church_holding

    # History

}
2777 = {

    # Misc
    holding = none

    # History

}
2778 = {

    # Misc
    holding = none

    # History

}

##d_kaldrland
###c_kaldrland
969 = {		#Kaldrborg

    # Misc
    culture = dalric
    religion = old_gerudian_faith
    holding = castle_holding

    # History
}
2779 = {

    # Misc
    holding = city_holding

    # History

}
2780 = {

    # Misc
    holding = none

    # History

}

###c_elgrbeiting
998 = {		#Elgrbeiting

    # Misc
    culture = dalric
    religion = old_gerudian_faith
    holding = castle_holding

    # History
}
2781 = {

    # Misc
    holding = none

    # History

}

##d_alptborg
###c_alptborg
957 = {		#Alptborg

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2782 = {

    # Misc
    holding = city_holding

    # History

}
2783 = {

    # Misc
    holding = none

    # History

}
2784 = {

    # Misc
    holding = none

    # History

}

###c_burrholt
964 = {		#Burrholt

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2785 = {

    # Misc
    holding = church_holding

    # History

}
2786 = {

    # Misc
    holding = none

    # History

}
2787 = {

    # Misc
    holding = none

    # History

}

##d_sidaett
###c_sidaett
967 = {		#Sidhalda

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
966 = {		#Silfrhof

    # Misc
	holding = city_holding

    # History
}
2788 = {

    # Misc
    holding = church_holding

    # History

}
2789 = {

    # Misc
    holding = none

    # History

}
2790 = {

    # Misc
    holding = none

    # History

}
2791 = {

    # Misc
    holding = none

    # History

}

###c_vargrhol
965 = {		#Vargrhol

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2792 = {

    # Misc
    holding = church_holding

    # History

}
2793 = {

    # Misc
    holding = none

    # History

}
2794 = {

    # Misc
    holding = none

    # History

}

##d_ismark
###c_algrar
970 = {		#Algrar

    # Misc
    culture = dalric
    religion = elkaesal_dragon_cult
    holding = castle_holding

    # History
}
2795 = {

    # Misc
    holding = none

    # History

}
2796 = {

    # Misc
    holding = none

    # History

}
2797 = {

    # Misc
    holding = none

    # History

}

###c_ljotrturf
968 = {		#Ljotrturf

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2798 = {

    # Misc
    holding = none

    # History

}
2799 = {

    # Misc
    holding = none

    # History

}

###c_grymskali
971 = {		#Grymskali

    # Misc
    culture = dalric
    religion = skaldhyrric_faith
    holding = castle_holding

    # History
}
2800 = {

    # Misc
    holding = church_holding

    # History

}
2801 = {

    # Misc
    holding = none

    # History

}
2802 = {

    # Misc
    holding = none

    # History

}
2803 = {

    # Misc
    holding = none

    # History

}