#k_sorncost
##d_sorncost
###c_sorncost
88 = {		#Sornaire

    # Misc
    culture = sorncosti
    religion = eidoueni
    holding = castle_holding

    # History
}
85 = {		#Sornbay

    # Misc
    holding = city_holding

    # History
}
1084 = {

    # Misc
    holding = none

    # History

}
1085 = {

    # Misc
    holding = none

    # History

}
1086 = {

    # Misc
    holding = church_holding

    # History

}
1087 = {

    # Misc
    holding = castle_holding

    # History

}

###c_casna_ola
90 = {		#Casna Ola

    # Misc
    culture = sormanni
    religion = eidoueni
    holding = castle_holding

    # History
}
1091 = {

    # Misc
    holding = none

    # History

}

###c_fioncavin
99 = {		#Fioncavin

    # Misc
    culture = sormanni
    religion = eidoueni
    holding = castle_holding

    # History
}
1088 = {

    # Misc
    holding = city_holding

    # History

}
1089 = {

    # Misc
    holding = none

    # History

}
1090 = {

    # Misc
    holding = none

    # History

}

##d_sormanni_hills
###c_aishill
91 = {		#Aishill

    # Misc
    culture = sormanni
    religion = eidoueni
    holding = castle_holding

    # History
}
1079 = {

    # Misc
    holding = none

    # History

}
1080 = {

    # Misc
    holding = church_holding

    # History

}

###c_aelhill
100 = {		#Aelhill

    # Misc
    culture = sormanni
    religion = eidoueni
    holding = castle_holding

    # History
}
1081 = {

    # Misc
    holding = none

    # History

}
1082 = {

    # Misc
    holding = none

    # History

}
1083 = {

    # Misc
    holding = city_holding

    # History

}

##d_coruan
###c_coruan
84 = {		#Coruan

    # Misc
    culture = sormanni
    religion = eidoueni
    holding = castle_holding

    # History

}
86 = {		#Bottlepoint

    # Misc
    holding = city_holding

    # History
}
1076 = {

    # Misc
    holding = none

    # History

}

###c_ebenuel
89 = {		#Ebenuel

    # Misc
    culture = sormanni
    religion = eidoueni
    holding = castle_holding

    # History
}
1077 = {

    # Misc
    holding = church_holding

    # History

}
1078 = {

    # Misc
    holding = none

    # History

}

##d_venail
###c_venail
93 = {		#Venail

    # Misc
    culture =  moon_elvish
    religion = elven_forebears
    holding = castle_holding

    # History
}
1103 = {

    # Misc
    holding = church_holding

    # History

}
1105 = {

    # Misc
    holding = city_holding

    # History

}
1102 = {

    # Misc
    holding = none

    # History

}

###c_firstsight
127 = {		#Firstsight

    # Misc
    culture =  moon_elvish
    religion = elven_forebears
    holding = castle_holding

    # History
}
1104 = {

    # Misc
    holding = none

    # History

}
1100 = {

    # Misc
    holding = city_holding

    # History

}
1101 = {

    # Misc
    holding = none

    # History

}

###c_edilliande
94 = {		#Edilliande

    # Misc
    culture =  moon_elvish
    religion = elven_forebears
    holding = castle_holding

    # History
}
1098 = {

    # Misc
    holding = none

    # History

}
1099 = {

    # Misc
    holding = none

    # History

}
