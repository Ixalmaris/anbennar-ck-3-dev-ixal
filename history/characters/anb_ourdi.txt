﻿
aldwigard0001 = { # Alfrede of Aldwigard, King of Ourdia
	name = "Alfrede"
	dynasty = dynasty_aldwigard
	religion = southern_cult_of_castellos
	culture = ourdi
	
	diplomacy = 14
	martial = 10
	stewardship = 16
	intrigue = 12
	learning = 9
	prowess = 8
	
	trait = race_human
	trait = education_stewardship_3
	trait = content
	trait = patient
	trait = diligent
	trait = physique_bad_1
	
	955.11.12 = {
		birth = yes
	}

	975.4.2 = {
		add_spouse = aldwigard0005
	}
}

aldwigard0002 = { # Trianu of Aldwigard
	name = "Trianu"
	dynasty = dynasty_aldwigard
	religion = southern_cult_of_castellos
	culture = ourdi
	
	diplomacy = 6
	martial = 14
	stewardship = 11
	intrigue = 8
	learning = 12
	prowess = 12

	father = aldwigard0001
	mother = aldwigard0005
	
	trait = race_human
	trait = education_martial_2
	trait = arrogant
	trait = brave
	trait = vengeful
	trait = journaller
	trait = one_legged
	
	979.5.6 = {
		birth = yes
	}
}

aldwigard0003 = { # Terin of Aldwigard
	name = "Terin"
	dynasty = dynasty_aldwigard
	religion = southern_cult_of_castellos
	culture = ourdi
	
	diplomacy = 6
	martial = 17
	stewardship = 8
	intrigue = 3
	learning = 8
	prowess = 17

	father = aldwigard0001
	mother = aldwigard0005
	
	trait = race_human
	trait = education_martial_3
	trait = honest
	trait = callous
	trait = wrathful
	
	984.1.2 = {
		birth = yes
	}
}

aldwigard0004 = { # Sofina of Aldwigard
	name = "Sofina"
	dynasty = dynasty_aldwigard
	religion = southern_cult_of_castellos
	culture = ourdi
	female = yes
	
	diplomacy = 7
	martial = 8
	stewardship = 16
	intrigue = 4
	learning = 12
	prowess = 8

	father = aldwigard0001
	mother = aldwigard0005
	
	trait = race_human
	trait = education_stewardship_2
	trait = chaste
	trait = forgiving
	trait = shy
	trait = lifestyle_gardener
	
	992.2.6 = {
		birth = yes
	}
}

aldwigard0005 = { # Ezebel of Aldwigard
	name = "Ezebel"
	religion = southern_cult_of_castellos
	culture = ourdi
	female = yes
	
	diplomacy = 12
	martial = 5
	stewardship = 8
	intrigue = 8
	learning = 10
	prowess = 6
	
	trait = race_human
	trait = education_diplomacy_1
	trait = temperate
	trait = ambitious
	trait = eccentric
	trait = lifestyle_herbalist
	trait = cancer
	
	958.8.5 = {
		birth = yes
	}
	1007.9.2 = {
		death = yes
	}
}

aldwigard0006 = { # Tiren of Aldwigard
	name = "Tiren"
	dynasty = dynasty_aldwigard
	religion = southern_cult_of_castellos
	culture = ourdi
	
	diplomacy = 11
	martial = 5
	stewardship = 5
	intrigue = 7
	learning = 7
	prowess = 2

	father = aldwigard0003
	mother = aldwigard0010
	
	trait = race_human
	trait = education_learning_2
	trait = impatient
	trait = gregarious
	trait = eccentric
	
	1002.11.25 = {
		birth = yes
	}
}

aldwigard0007 = { # Trystan of Aldwigard
	name = "Trystan"
	dynasty = dynasty_aldwigard
	religion = southern_cult_of_castellos
	culture = ourdi
	
	diplomacy = 12
	martial = 5
	stewardship = 17
	intrigue = 9
	learning = 12
	prowess = 5

	father = aldwigard0002
	mother = urzhana_0002
	
	trait = race_human
	trait = education_stewardship_3
	trait = diligent
	trait = callous
	trait = humble
	trait = administrator
	
	998.12.3 = {
		birth = yes
	}
}

aldwigard0008 = { # Nara of Aldwigard
	name = "Nara"
	dynasty = dynasty_aldwigard
	religion = southern_cult_of_castellos
	culture = ourdi
	female = yes
	
	diplomacy = 11
	martial = 4
	stewardship = 8
	intrigue = 17
	learning = 7
	prowess = 5

	father = aldwigard0002
	mother = urzhana_0002
	
	trait = race_human
	trait = education_intrigue_4
	trait = stubborn
	trait = impatient
	trait = lustful
	
	1003.9.17 = {
		birth = yes
	}
}

urzhana_0002 = { # Ariana of Aldwigard
	name = "Ariana"
	dynasty = dynasty_urzhana
	religion = cult_of_the_citadel
	culture = yametsesi
	female = yes
	
	diplomacy = 14
	martial = 5
	stewardship = 6
	intrigue = 7
	learning = 8
	prowess = 3
	
	trait = race_human
	trait = education_diplomacy_2
	trait = calm
	trait = gregarious
	trait = trusting
	trait = seducer
	
	982.6.21 = {
		birth = yes
	}
}

aldwigard0010 = { # Anaisa of Aldwigard
	name = "Anaisa"
	dynasty = dynasty_aldwigard
	religion = southern_cult_of_castellos
	culture = ourdi
	female = yes
	
	diplomacy = 6
	martial = 3
	stewardship = 1
	intrigue = 11
	learning = 8
	prowess = 2
	
	trait = race_human
	trait = education_intrigue_3
	trait = lazy
	trait = fickle
	trait = gluttonous
	
	986.3.9 = {
		birth = yes
	}
}

ueleden0001 = { # Adrien of Ueleden
	name = "Adrien"
	religion = southern_cult_of_castellos
	culture = ourdi
	
	diplomacy = 7
	martial = 13
	stewardship = 9
	intrigue = 8
	learning = 6
	prowess = 18
	
	trait = race_human
	trait = education_martial_3
	trait = content
	trait = greedy
	trait = brave
	trait = strong
	
	992.5.13 = {
		birth = yes
	}
	1021.2.7 = {
		add_spouse = ueleden0002
	}
}

ueleden0002 = { # Cibila of Ueleden
	name = "Cibila"
	dynasty = dynasty_ueleden
	religion = southern_cult_of_castellos
	culture = ourdi
	female = yes
	
	diplomacy = 10
	martial = 4
	stewardship = 14
	intrigue = 7
	learning = 13
	prowess = 6
	
	trait = race_human
	trait = education_learning_2
	trait = craven
	trait = trusting
	trait = lazy
	trait = beauty_good_1
	
	994.8.9 = {
		birth = yes
	}
}

dostanesck0001 = { # Sandr of Dostanesck
	name = "Sandr"
	religion = southern_cult_of_castellos
	culture = ourdi
	
	diplomacy = 5
	martial = 15
	stewardship = 12
	intrigue = 2
	learning = 7
	prowess = 12
	
	trait = race_human
	trait = education_stewardship_2
	trait = vengeful
	trait = ambitious
	trait = just
	trait = logistician
	
	987.12.17 = {
		birth = yes
	}
	1021.6.7 = {
		add_spouse = aldwigard0004
	}
}

demonsfall0001 = { # Alin of Demonsfall
	name = "Alin"
	religion = southern_cult_of_castellos
	culture = ourdi
	
	diplomacy = 7
	martial = 19
	stewardship = 9
	intrigue = 5
	learning = 4
	prowess = 14
	
	trait = race_human
	trait = education_martial_3
	trait = temperate
	trait = stubborn
	trait = wrathful
	trait = reckless
	
	994.10.18 = {
		birth = yes
	}
	1021.6.7 = {
		add_spouse = demonsfall0002
	}
}

demonsfall0002 = { # Alice of Demonsfall
	name = "Alice"
	religion = southern_cult_of_castellos
	culture = ourdi
	female = yes
	
	diplomacy = 8
	martial = 7
	stewardship = 14
	intrigue = 6
	learning = 17
	prowess = 11
	
	trait = race_human
	trait = education_diplomacy_1
	trait = patient
	trait = humble
	trait = shy
	trait = magical_affinity_1
	
	994.10.18 = {
		birth = yes
	}
}