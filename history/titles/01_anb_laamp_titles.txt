﻿
d_laamp_narawen = {
	1014.1.1 = {
		liege = 0
		holder = 16 # Narawen
		government = landless_adventurer_government
		succession_laws = { landless_adventurer_succession_law }
		effect = {
			create_landless_adventurer_title_history_effect = yes
			set_variable = { name = adventurer_creation_reason value = flag:historical }
			destroy_landless_title_no_dlc_effect = { DATE = 1200.1.1 } # placeholder date, idk
		}
	}
}
